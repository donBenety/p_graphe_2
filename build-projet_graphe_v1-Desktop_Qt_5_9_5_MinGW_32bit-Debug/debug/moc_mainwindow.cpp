/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../projet/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_mainWindow_t {
    QByteArrayData data[21];
    char stringdata0[299];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_mainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_mainWindow_t qt_meta_stringdata_mainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "mainWindow"
QT_MOC_LITERAL(1, 11, 13), // "chargerGraphe"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 17), // "sauvegarderGraphe"
QT_MOC_LITERAL(4, 44, 13), // "nouveauGraphe"
QT_MOC_LITERAL(5, 58, 16), // "setAjouterSommet"
QT_MOC_LITERAL(6, 75, 18), // "setSupprimerSommet"
QT_MOC_LITERAL(7, 94, 13), // "setAjouterArc"
QT_MOC_LITERAL(8, 108, 15), // "setSupprimerArc"
QT_MOC_LITERAL(9, 124, 10), // "setQuitter"
QT_MOC_LITERAL(10, 135, 17), // "setEnregistrement"
QT_MOC_LITERAL(11, 153, 18), // "afficherMatriceAdj"
QT_MOC_LITERAL(12, 172, 20), // "afficherMatriceCouts"
QT_MOC_LITERAL(13, 193, 4), // "rang"
QT_MOC_LITERAL(14, 198, 9), // "distances"
QT_MOC_LITERAL(15, 208, 16), // "dessinerDijkstra"
QT_MOC_LITERAL(16, 225, 14), // "dessinerTarjan"
QT_MOC_LITERAL(17, 240, 16), // "redessinerGraphe"
QT_MOC_LITERAL(18, 257, 11), // "coderPrufer"
QT_MOC_LITERAL(19, 269, 13), // "decoderPrufer"
QT_MOC_LITERAL(20, 283, 15) // "dessinerKruskal"

    },
    "mainWindow\0chargerGraphe\0\0sauvegarderGraphe\0"
    "nouveauGraphe\0setAjouterSommet\0"
    "setSupprimerSommet\0setAjouterArc\0"
    "setSupprimerArc\0setQuitter\0setEnregistrement\0"
    "afficherMatriceAdj\0afficherMatriceCouts\0"
    "rang\0distances\0dessinerDijkstra\0"
    "dessinerTarjan\0redessinerGraphe\0"
    "coderPrufer\0decoderPrufer\0dessinerKruskal"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_mainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x0a /* Public */,
       3,    0,  110,    2, 0x0a /* Public */,
       4,    0,  111,    2, 0x0a /* Public */,
       5,    0,  112,    2, 0x0a /* Public */,
       6,    0,  113,    2, 0x0a /* Public */,
       7,    0,  114,    2, 0x0a /* Public */,
       8,    0,  115,    2, 0x0a /* Public */,
       9,    0,  116,    2, 0x0a /* Public */,
      10,    0,  117,    2, 0x0a /* Public */,
      11,    0,  118,    2, 0x0a /* Public */,
      12,    0,  119,    2, 0x0a /* Public */,
      13,    0,  120,    2, 0x0a /* Public */,
      14,    0,  121,    2, 0x0a /* Public */,
      15,    0,  122,    2, 0x0a /* Public */,
      16,    0,  123,    2, 0x0a /* Public */,
      17,    0,  124,    2, 0x0a /* Public */,
      18,    0,  125,    2, 0x0a /* Public */,
      19,    0,  126,    2, 0x0a /* Public */,
      20,    0,  127,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void mainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        mainWindow *_t = static_cast<mainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->chargerGraphe(); break;
        case 1: _t->sauvegarderGraphe(); break;
        case 2: _t->nouveauGraphe(); break;
        case 3: _t->setAjouterSommet(); break;
        case 4: _t->setSupprimerSommet(); break;
        case 5: _t->setAjouterArc(); break;
        case 6: _t->setSupprimerArc(); break;
        case 7: _t->setQuitter(); break;
        case 8: _t->setEnregistrement(); break;
        case 9: _t->afficherMatriceAdj(); break;
        case 10: _t->afficherMatriceCouts(); break;
        case 11: _t->rang(); break;
        case 12: _t->distances(); break;
        case 13: _t->dessinerDijkstra(); break;
        case 14: _t->dessinerTarjan(); break;
        case 15: _t->redessinerGraphe(); break;
        case 16: _t->coderPrufer(); break;
        case 17: _t->decoderPrufer(); break;
        case 18: _t->dessinerKruskal(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject mainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_mainWindow.data,
      qt_meta_data_mainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *mainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *mainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_mainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int mainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
