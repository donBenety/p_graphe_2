#ifndef COUT_H
#define COUT_H

#include <QtGui>
#include <QGraphicsTextItem>

#include "graphe.h"
#include "grapheOV.h"
#include "grapheNOV.h"
#include "grapheONV.h"
#include "grapheNONV.h"

class WGraphe;

class couts : public QGraphicsTextItem
{
public:
    couts(WGraphe *parent, int cout, int sommet1, int sommet2);
    virtual ~couts();
    //evenement de la souris
    void mouseListener(QGraphicsSceneMouseEvent *e);

private:
    int d_cout;
    WGraphe *d_parent;
    // cout d'un arc
    int sommet_dep, sommet_arr;
};
#endif
