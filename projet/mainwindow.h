#ifndef PROJETGRAPHE_H
#define PROJETGRAPHE_H

#include <QtWidgets/QMainWindow>
#include <QPushButton>
#include <QCheckBox>
#include <QPainter>

#include <QVBoxLayout>

#include "ui_mainwindow.h"

#include "WGraphe.h"
#include "graphe.h"
#include "grapheOV.h"
#include "grapheNONV.h"
#include "grapheNOV.h"
#include "grapheONV.h"


class ProjetGraphe : public QMainWindow
{
        Q_OBJECT
    public:
        ProjetGraphe();
        virtual ~ProjetGraphe();

        void afficherTab(int *tab);

        public slots:
        //menu fichier
        void chargerGraphe();
        void sauvegarderGraphe();
        void nouveauGraphe();

        //dockwidget
        void setAjoutSom();
        void setSupprSom();
        void setAjoutArc();
        void setSupprArc();
        void setQuitter();
        //void setEnregistrement();

        //menu graphe
        void afficherMatriceAdj();
        void afficherMatriceCouts();
        void rang();
        void distances();
        void dessinerDijkstra();
        void dessinerTarjan();
        void redessinerGraphe();
        void coderPrufer();
        void decoderPrufer();
        void dessinerKruskal();

      //  void afficherOutils();

    private:
        //widget du graphe
        WGraphe *wid;
        //widget et table pour l'affichage des distances
        QWidget *w;
        QTableWidget *table;

        //élement de la boite à outils
        QDockWidget *dock;
        QDockWidget *dock_above;
        QWidget *dockwid;
        QWidget *dockwid_above;
        QPushButton *ajoutsom, *supprsom, *ajoutarc, *supprarc, *nouveau;
        QPushButton *quitter, *charger;
        QCheckBox *value, *oriente;
        QHBoxLayout *dockLayout;
        QVBoxLayout *dockLayout_above;

        //élements du menu
        QMenu *fichier;
        QMenu *graph;
        QMenu *apropos;
        QMenu *outils;


        //action des menu
        //QAction *quitter;
        QAction *nveau;
        QAction *propos;
       // QAction *charger;
        QAction *sauvegarder;
        QAction *barre_outils;

        QAction *dessiner;
        QAction *matrice;
        QAction *couts;
        QAction *rg;
        QAction *dist;
        QAction *Dijkstra;
        QAction *Tarjan;
        QAction *prufer;
        QAction *kruskal;
        QAction *decoderprufer;
        QAction *ordo;

        //largeur et hauteur de la fenetre
        static const int HAUT = 900, LARG = 800;
};

#endif // PROJETGRAPHE_H
