#include <QtGui>
#include <fstream>
#include "mainwindow.h" //ProjetGraphe
#include <QDockWidget>
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>

ProjetGraphe::ProjetGraphe() : QMainWindow(), fichier(nullptr), graph(nullptr), ajoutsom(new QPushButton("Ajouter sommet")),
supprsom(new QPushButton("Supprimer sommet")), ajoutarc(new QPushButton("Ajouter arc")), quitter(new QPushButton("Quitter")), charger(new QPushButton("Charger")),
supprarc(new QPushButton("Supprimer arc")), nouveau(new QPushButton("Nouveau")),
value(new QCheckBox("Valué")), oriente(new QCheckBox("Orienté")), apropos(nullptr), propos(nullptr)
{
    wid = new WGraphe(this);
    this->resize(HAUT, LARG);
    this->setWindowTitle("Algorithme & Graphe");


     wid->setStyleSheet("background-image:url(C:/Users/okapi/Documents/projet_graphe_v1/projet_graphe_v1/scene.jpg);");
    setCentralWidget(wid);

    fichier = menuBar()->addMenu("&Fichier");
    graph = menuBar()->addMenu("& Algorithme G");
    apropos = menuBar()->addMenu("&?");
    outils = menuBar()->addMenu("&Outils");

    nveau = fichier->addAction("&Nouveau");
    //charger = fichier->addAction("&Charger");
    sauvegarder = fichier->addAction("&Sauvegarder");

    fichier->addSeparator();
   // quitter = fichier->addAction("&Quitter");
    propos = apropos->addAction("&Qt");

    outils->addSeparator();
    barre_outils = outils->addAction("&Barre outils");




    matrice = graph->addAction("&Matrice d'adjacence");
    couts = graph->addAction("&Matrice des couts");
    graph->addSeparator();
    dessiner = graph->addAction("&Redessiner graphe");
    graph->addSeparator();
    rg = graph->addAction("&Calcul du rang");
    dist = graph->addAction("&Distances");
    Dijkstra = graph->addAction("&Dijkstra");
    Tarjan = graph->addAction("&Tarjan");
    kruskal = graph->addAction("&Kruskal");
    ordo = graph->addAction("&Ordonnancement");
    graph->addSeparator();
    prufer = graph->addAction("&Coder Prüfer");
    decoderprufer = graph->addAction("&Décoder Prüfer");




    QObject::connect(propos, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    QObject::connect(sauvegarder, SIGNAL(triggered()), this, SLOT(sauvegarderGraphe()));
    QObject::connect(nveau, SIGNAL(triggered()), this, SLOT(nouveauGraphe()));



    ajoutsom->setCheckable(true);
    supprsom->setCheckable(true);
    ajoutarc->setCheckable(true);
    supprarc->setCheckable(true);
    //charger->setCheckable(true);
    //enregistrement->setCheckable(true);
    quitter->setCheckable(true);
    value->setDisabled(true);
    oriente->setDisabled(true);

    QObject::connect(Dijkstra, SIGNAL(triggered()), this, SLOT(dessinerDijkstra()));
    QObject::connect(dessiner, SIGNAL(triggered()), this, SLOT(redessinerGraphe()));
    QObject::connect(matrice, SIGNAL(triggered()), this, SLOT(afficherMatriceAdj()));
    QObject::connect(couts, SIGNAL(triggered()), this, SLOT(afficherMatriceCouts()));
    QObject::connect(rg, SIGNAL(triggered()), this, SLOT(rang()));
    QObject::connect(dist, SIGNAL(triggered()), this, SLOT(distances()));
    QObject::connect(prufer, SIGNAL(triggered()), this, SLOT(coderPrufer()));
    QObject::connect(kruskal, SIGNAL(triggered()), this, SLOT(dessinerKruskal()));
    QObject::connect(decoderprufer, SIGNAL(triggered()), wid, SLOT(lirePrufer()));
    QObject::connect(ordo, SIGNAL(triggered()), wid, SLOT(ordonnancement()));

    dock = new QDockWidget("Outils", this);
    dock->setFloating(false);
    dock->setFeatures(QDockWidget::NoDockWidgetFeatures | QDockWidget::DockWidgetFloatable);
    addDockWidget(Qt::TopDockWidgetArea, dock);
    dockwid = new QWidget;
    dock->setWidget(dockwid);
    dockLayout = new QHBoxLayout;
    dockLayout->addWidget(value);
    dockLayout->addWidget(oriente);
    dockLayout->addWidget(ajoutsom);
    dockLayout->addWidget(supprsom);
    dockLayout->addWidget(ajoutarc);
   // dockLayout->addWidget(quitter);
    dockLayout->addWidget(supprarc);
    dockLayout->insertSpacing(6, 20);
    //dockLayout->addWidget(enregistrement);
    dockwid->setLayout(dockLayout);


    /*here above */
    dock_above = new QDockWidget("barre", this);
    dock_above->setFloating(false);
    dock_above->setFeatures(QDockWidget::NoDockWidgetFeatures | QDockWidget::DockWidgetFloatable);
    addDockWidget(Qt::LeftDockWidgetArea , dock_above);
    dockwid_above = new QWidget;
    dock_above->setWidget(dockwid_above);
    dockLayout_above = new QVBoxLayout;

    dockLayout_above->addWidget(quitter);
    dockLayout_above->addWidget(nouveau);
    dockLayout_above->addWidget(charger);
    dockwid_above->setLayout(dockLayout_above);

    QObject::connect(quitter, SIGNAL(clicked()), this, SLOT(setQuitter()));
    QObject::connect(charger, SIGNAL(clicked()), this, SLOT(chargerGraphe()));
    QObject::connect(nouveau, SIGNAL(clicked()), this, SLOT(nouveauGraphe()));
    QObject::connect(ajoutsom, SIGNAL(clicked()), this, SLOT(setAjoutSom()));
    QObject::connect(supprsom, SIGNAL(clicked()), this, SLOT(setSupprSom()));
    QObject::connect(ajoutarc, SIGNAL(clicked()), this, SLOT(setAjoutArc()));
    QObject::connect(supprarc, SIGNAL(clicked()), this, SLOT(setSupprArc()));
    //QObject::connect(enregistrement, SIGNAL(clicked()), this, SLOT(setEnregistrement()));

    QPalette p;
   // QPixmap img ("C:/Users/okapi/Documents/projet_graphe_v1/projet_graphe_v1/graphe2.png");
   // img = img.scaled(this->size(), Qt::IgnoreAspectRatio);
   //  QPalette palette;
   //  palette.setBrush(QPalette::Background, img);
    p.setBrush(QPalette::Background, Qt::gray);
    //dockwid->setStyleSheet("background-color:black;");
    // p.setColor(QPalette::Background, Qt::yellow);
    setPalette(p);

}

ProjetGraphe::~ProjetGraphe()
{
    //delete w ;
    //delete table ;

    delete wid;
    delete ajoutsom;
    delete supprsom;
    delete ajoutarc;
    delete supprarc;
    delete value;
    delete oriente;
    delete dockLayout;
    delete dockwid;
    delete dock;
    delete quitter;

    delete fichier;
    delete graph;
    delete apropos;
}

void ProjetGraphe::afficherTab(int *d)
{
    w = new QWidget();
    QVBoxLayout *l = new QVBoxLayout;
    w->setWindowTitle("Distances");
    table = new QTableWidget(w);

    int n = d[0];
    /*
    QTableWidgetItem* t ;
    for(int i=0 ; i<n ; ++i)
    {
    t = new QTableWidgetItem(wid->complexe[i]) ;
    table->setHorizontalHeaderItem(0,t) ;
    }
    */
    table->setRowCount(1);
    table->setColumnCount(n);
    table->resize(110 * n, 50);
    for (int i = 1; i <= n; ++i)
        table->setItem(0, i - 1, new QTableWidgetItem(QString::number(d[i])));

    //t = new QTableWidgetItem("Intitulé") ;
    w->setLayout(l);
    l->addWidget(table);
    w->show();
}

void ProjetGraphe::chargerGraphe()
{

    if (wid->gcourant != nullptr)
    {
        int rep = QMessageBox::warning(this, "Charger un graphe", "Supprimmer le graphe courant ?", QMessageBox::Yes | QMessageBox::No);

        if (rep == QMessageBox::No)
            return;
    }
    delete wid->gcourant;
    wid->gcourant = nullptr;
    QString f = QFileDialog::getOpenFileName(this, "Graphe à ouvrir");
    ifstream is(f.toStdString().c_str());
    graphe *gcourant = graphe::lire(is);
    wid->setGraphe(gcourant);

    ifstream is2((f.toStdString() + "complexe").c_str());
    int n;
    is2 >> n;
    wid->complexe.resize(n);
    string tmp;
    for (int i = 0; i<n; ++i)
    {
        is2 >> tmp;
        wid->complexe[i] = tmp.c_str();
    }

    grapheOV *gOV = dynamic_cast<grapheOV *>(gcourant);
    if (gOV != nullptr)
    {
        value->setCheckState(Qt::Checked);
        oriente->setCheckState(Qt::Checked);
    }

    grapheNONV *gNONV = dynamic_cast<grapheNONV *>(gcourant);
    if (gNONV != nullptr)
    {
        value->setCheckState(Qt::Unchecked);
        oriente->setCheckState(Qt::Unchecked);
    }
    grapheNOV *gNOV = dynamic_cast<grapheNOV *>(gcourant);
    if (gNOV != nullptr)
    {
        value->setCheckState(Qt::Checked);
        oriente->setCheckState(Qt::Unchecked);
    }
    grapheONV *gONV = dynamic_cast<grapheONV *>(gcourant);
    if (gONV != nullptr)
    {
        value->setCheckState(Qt::Unchecked);
        oriente->setCheckState(Qt::Checked);
    }
    wid->value = value->isChecked();
    wid->oriente = oriente->isChecked();

    wid->dessiner();
}

void ProjetGraphe::sauvegarderGraphe()
{

    if (wid->gcourant == nullptr)
    {
        //QMessageBox(QMessageBox::Information,"Sauvegarder un graphe", "Impossible de sauvegarder un graphe vide",QMessageBox::Ok) ;
        QMessageBox::information(this, "Sauvegarder un graphe", "Impossible de sauvegarder un graphe vide", QMessageBox::Ok);
        return;
    }

    QString f = QFileDialog::getSaveFileName(this, "Sauvegarder un graphe", "graphe.g");
    ofstream os(f.toStdString().c_str());
    graphe::ecrire(*(wid->gcourant), os);
    os.close();

    ofstream os2((f.toStdString() + "complexe").c_str());
    int n = wid->gcourant->nbSommet();
    os2 << n << std::endl;

    for (int i = 0; i<(wid->item).size(); ++i)
        os2 << ((wid->item[i])->info()).toStdString() << " ";
    os2.close();

}

void ProjetGraphe::nouveauGraphe()
{
    int rep = QMessageBox::warning(this, "Créé un nouveau graphe", "initialiser la scène ?", QMessageBox::Yes | QMessageBox::No);

    if (rep == QMessageBox::No)
        return;

    bool ok;
    QStringList items;
    items << tr("Orienté Valué") << tr("Orienté Non Valué") << tr("Non Orienté Valué") << tr("Non OrientéNon Valué");
    QString item = QInputDialog::getItem(this, tr("QInputDialog::getItem()"), tr("Type de graphe"), items, 0, false, &ok);

    if (!ok)
        return;

    delete wid->gcourant;

    if (item == "Orienté Valué")
    {
        wid->gcourant = new grapheOV();
        value->setCheckState(Qt::Checked);
        wid->value = true;
        oriente->setCheckState(Qt::Checked);
        wid->oriente = true;
    }
    else if (item == "Orienté Non Valué")
    {
        wid->gcourant = new grapheONV();
        value->setCheckState(Qt::Unchecked);
        oriente->setCheckState(Qt::Checked);
        wid->value = false;
        wid->oriente = true;

    }
    else if (item == "Non Orienté Valué")
    {
        wid->gcourant = new grapheNOV();
        value->setCheckState(Qt::Checked);
        oriente->setCheckState(Qt::Unchecked);
        wid->value = true;
        wid->oriente = false;
    }
    else
    {
        wid->gcourant = new grapheNONV();
        value->setCheckState(Qt::Unchecked);
        oriente->setCheckState(Qt::Unchecked);
        wid->value = false;
        wid->oriente = false;
    }

    wid->aa = false;
    ajoutarc->setChecked(false);
    wid->as = false;
    ajoutsom->setChecked(false);
    wid->ss = false;
    supprsom->setChecked(false);
    wid->sa = false;
    supprarc->setChecked(false);

    wid->complexe.resize(0);
    wid->dessiner();
}

void ProjetGraphe::setAjoutSom()
{
    if (ajoutsom->isChecked())
    {
        wid->as = true;
        wid->aa = false;
        wid->ss = false;
        wid->sa = false;
        wid->e = false;
        supprsom->setChecked(false);
        supprarc->setChecked(false);
        ajoutarc->setChecked(false);
       // enregistrement->setChecked(false);
    }
    else
        wid->as = false;

}

void ProjetGraphe::setQuitter()
{
    if (quitter->isChecked())
    {
       close();
    }
}

void ProjetGraphe::setSupprSom()
{
    if (supprsom->isChecked())
    {
        wid->as = false;
        wid->aa = false;
        wid->ss = true;;
        wid->sa = false;
        ajoutsom->setChecked(false);
        supprarc->setChecked(false);
        ajoutarc->setChecked(false);
        wid->e = false;
        //enregistrement->setChecked(false);
        wid->dep = wid->arr = 0;
    }
    else
        wid->ss = false;
}
void ProjetGraphe::setAjoutArc()
{
    if (ajoutarc->isChecked())
    {
        wid->as = false;
        wid->aa = true;
        wid->ss = false;
        wid->sa = false;
        supprsom->setChecked(false);
        supprarc->setChecked(false);
        ajoutsom->setChecked(false);
        wid->e = false;
        //enregistrement->setChecked(false);
        wid->dep = wid->arr = 0;
    }
    else
        wid->aa = false;
}
void ProjetGraphe::setSupprArc()
{
    if (supprarc->isChecked())
    {
        wid->as = false;
        wid->aa = false;
        wid->ss = false;
        wid->sa = true;
        supprsom->setChecked(false);
        ajoutsom->setChecked(false);
        ajoutarc->setChecked(false);
        wid->e = false;
       // enregistrement->setChecked(false);
        wid->dep = wid->arr = 0;
    }
    else
        wid->sa = false;
}



void ProjetGraphe::dessinerDijkstra()
{
    wid->dessiner();

    grapheOV *gOV = nullptr;
    grapheNOV *gNOV = nullptr;
    gOV = dynamic_cast<grapheOV *>(wid->gcourant);
    gNOV = dynamic_cast<grapheNOV *>(wid->gcourant);

    if (gOV == nullptr && gNOV == nullptr)
    {
        QMessageBox::information(this, "Algorithme de Dijkstra", "L' algo de Dyjksta ne s'execute que sur un graphe valué", QMessageBox::Ok);
        return;
    }

    bool ok;
    int rep = QInputDialog::getInt(nullptr, QString("Dijkstra"), QString("Sommet de départ ? :"), 1, 1, wid->gcourant->nbSommet(), 1, &ok);
    int *d = nullptr, *pere = nullptr;
    if (gOV != nullptr)
        gOV->Dijkstra(rep, d, pere);
    else
        if (gNOV != nullptr)
            gNOV->Dijkstra(rep, d, pere);

    int n = pere[0];
    for (int i = 1; i <= n; ++i)
    {
        if (pere[i] != 0)
        {
            wid->colorierArc(pere[i], i, QColor(Qt::red));
        }
    }
    afficherTab(d);
}

void ProjetGraphe::dessinerTarjan()
{
    wid->dessiner();

    //grapheOV *gOV = nullptr;
    grapheONV *gONV = nullptr;
   // gOV = dynamic_cast<grapheOV *>(wid->gcourant);
    gONV = dynamic_cast<grapheONV *>(wid->gcourant);

    if (gONV == nullptr)
    {
        QMessageBox::information(this, "Algorithme de Tarjan", "L' algo de Tarjan ne s'execute que sur un graphe valué", QMessageBox::Ok);
        return;
    }

    bool ok;
    int rep = QInputDialog::getInt(nullptr, QString("Tarjan"), QString("Sommet de départ ? :"), 1, 1, wid->gcourant->nbSommet(), 1, &ok);
    int *d = nullptr, *pere = nullptr;

    if (gONV != nullptr)
     {
       // gONV->Tarjan(NULL, d, pere);
     }

    int n = pere[0];
    for (int i = 1; i <= n; ++i)
    {
        if (pere[i] != 0)
        {
            wid->colorierArc(pere[i], i, QColor(Qt::red));
        }
    }
    afficherTab(d);
}

void ProjetGraphe::redessinerGraphe()
{
    wid->dessiner();
}

void ProjetGraphe::afficherMatriceAdj()
{
    if (wid->gcourant == nullptr)
        return;

    int **adj = wid->gcourant->fs2adj();

    if (adj == nullptr)
        return;

    int n = adj[0][0];

    QWidget *w = new QWidget();
    QVBoxLayout *Layout = new QVBoxLayout;
    w->setWindowTitle("Matrice d'adjacence");
    QTableWidget *table = new QTableWidget(w);
    table->setRowCount(n);
    table->setColumnCount(n);
    table->resize(110 * n, 40 * n);
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            table->setItem(i - 1, j - 1, new QTableWidgetItem(QString::number(adj[i][j])));
    Layout->addWidget(table);
    w->setLayout(Layout);
    w->show();
}

void ProjetGraphe::afficherMatriceCouts()
{
    if (wid->gcourant == nullptr)
        return;

    grapheOV *gOV = nullptr;
    gOV = dynamic_cast<grapheOV *>(wid->gcourant);
    grapheNOV *gNOV = nullptr;
    gNOV = dynamic_cast<grapheNOV *>(wid->gcourant);

    if (gNOV == nullptr && gOV == nullptr)
    {
        QMessageBox::information(this, "Matrice des couts", "Le graphe n'est pas valué", QMessageBox::Ok);
        return;
    }

    int **c = nullptr;

    if (gNOV != nullptr)
        c = gNOV->couts();
    else
        c = gOV->couts();

    if (c == nullptr)
        return;

    int n = c[0][0];

    QWidget *w = new QWidget();
    QVBoxLayout *Layout = new QVBoxLayout;
    w->setWindowTitle("Matrice des couts");
    QTableWidget *table = new QTableWidget(w);
    table->setRowCount(n);
    table->setColumnCount(n);
    table->resize(105 * n, 35 * n);
    int nb;
    QString str;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
        {
            nb = c[i][j];
            if (nb == INT_MAX)
                str = "/";
            else
                str = QString::number(nb);
            table->setItem(i - 1, j - 1, new QTableWidgetItem(str));
        }
    Layout->addWidget(table);
    w->setLayout(Layout);
    w->show();
}

void ProjetGraphe::rang()
{
    if (wid->gcourant == nullptr)
        return;

    grapheOV *gOV = dynamic_cast<grapheOV *>(wid->gcourant);
    grapheONV *gONV = dynamic_cast<grapheONV *>(wid->gcourant);

    if (gOV == nullptr && gONV == nullptr)
    {
        QMessageBox::information(this, "Algorithme du rang", "L'algorithme du rang ne s'execute que sur un graphe orienté", QMessageBox::Ok);
        return;
    }

    int *rg = nullptr, *pilch, *prem;

    if (gOV != nullptr)
        rg = gOV->rang(pilch, prem);
    else
        rg = gONV->rang(pilch, prem);

    if (rg == nullptr)
        return;

    int n = rg[0];
    QWidget *w = new QWidget();
    w->setWindowTitle("rang des sommets du graphe");
    QTableWidget *table = new QTableWidget(w);
    table->setRowCount(1);
    table->setColumnCount(n);
    table->resize(110 * n, 50);
    for (int i = 1; i <= n; ++i)
        table->setItem(0, i - 1, new QTableWidgetItem(QString::number(rg[i])));
    w->show();
}



void ProjetGraphe::distances()
{
    if (wid->gcourant == nullptr)
        return;

    grapheOV *gOV = dynamic_cast<grapheOV *>(wid->gcourant);
    grapheONV *gONV = dynamic_cast<grapheONV *>(wid->gcourant);
    //grapheNOV *gNOV = dynamic_cast<grapheNOV *>(wid->gcourant) ;

    if (gOV == nullptr && gONV == nullptr /*&& gNOV == nullptr*/)
    {
        QMessageBox::information(this, "Calcul des distances", "On ne peut calculer les distances que sur un graphe orienté", QMessageBox::Ok);
        return;
    }
    int *dist = nullptr;
    int n = wid->gcourant->nbSommet();
    bool ok;
    int rep = rep = QInputDialog::getInt(nullptr, QString("Distances"), QString("Calcul des distances pour le sommet ? :"), 1, 1, n, 1, &ok);

    if (!ok)
        return;

    if (gOV != nullptr)
        dist = gOV->dist(rep);
    else if (gONV != nullptr)
        dist = gONV->dist(rep);
    /*
    else
    dist = gNOV->dist(rep) ;
    */
    if (dist == nullptr)
        return;

    QWidget *w = new QWidget();
    QVBoxLayout *l = new QVBoxLayout();
    w->setWindowTitle("Distance du sommet" + QString::number(rep));
    w->setLayout(l);
    QTableWidget *table = new QTableWidget(w);
    //l->setWidget(w);
    table->setRowCount(1);
    table->setColumnCount(n);
    table->resize(110 * n, 50);
    for (int i = 1; i <= n; ++i)
        table->setItem(0, i - 1, new QTableWidgetItem(QString::number(dist[i])));
    l->addWidget(table);
    w->setLayout(l);
    w->show();
}

void ProjetGraphe::coderPrufer()
{
    if (wid->gcourant == nullptr)
        return;

    grapheNONV *gNONV = nullptr;
    gNONV = dynamic_cast<grapheNONV *>(wid->gcourant);

    if (gNONV == nullptr)
    {
        QMessageBox::information(this, "Codagede Prüfer", "Le codage de Prüfer ne s'execute que sur un graphe non-orienté et non-valué", QMessageBox::Ok);
        return;
    }
    int *d = nullptr;
    if (!gNONV->est_arbre(d))
    {
        QMessageBox::information(this, "Codage de Prüfer", "Le graphe n'est pas un arbre", QMessageBox::Ok);
        return;
    }
    if (d == nullptr)
        return;

    int n = d[0];

    QWidget *w = new QWidget();
    w->setWindowTitle("Codage de Prüfer");
    QTableWidget *table = new QTableWidget(w);
    table->setRowCount(1);
    table->setColumnCount(n);
    table->resize(110 * n, 50);
    for (int i = 1; i <= n; ++i)
        table->setItem(0, i - 1, new QTableWidgetItem(QString::number(d[i])));
    w->show();
}

void ProjetGraphe::decoderPrufer()
{

}



void ProjetGraphe::dessinerKruskal()
{
    wid->dessiner();

    if (wid->gcourant == nullptr)
        return;

    int **arete = nullptr;
    grapheNOV * gNOV = dynamic_cast<grapheNOV *>(wid->gcourant);
    if (gNOV == nullptr)
    {
        QMessageBox::information(this, "Algorithme de Kruskal", "L' algo de Kruskal ne s'execute que sur un graphe non-orienté et valué", QMessageBox::Ok);
        return;
    }
    gNOV->Kruskal(arete);

    if (arete == nullptr)
        return;

    int n = wid->gcourant->nbSommet();

    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= n; ++j)
            if (arete[i][j] == 1)
                wid->colorierArc(i, j, QColor(Qt::yellow));

    }

}



