#ifndef ARC_H
#define ARC_H

#include <QGraphicsLineItem>

class sommet;

//arête du graphe
class arc : public QGraphicsLineItem
{
    friend class WGraphe;

public:
    //constructeur de l'arc
    arc(sommet *sommet1, sommet *sommet2, bool oriente, QGraphicsScene* parent);

private:
    QGraphicsScene *d_parent;
    static const int TAILLE = 30, BLANC = 40;
    int sommet_dep, sommet_arr;
};

#endif
