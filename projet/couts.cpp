#include "couts.h"
#include "WGraphe.h"
#include <QGraphicsSceneMouseEvent>
#include <QInputDialog>

couts::couts(WGraphe *parent, int cout, int sommet1, int sommet2) :
QGraphicsTextItem(QString::number(cout)), d_cout(cout), d_parent(parent),
sommet_dep(sommet1), sommet_arr(sommet2)
{
}

couts::~couts()
{	}

void couts::mouseListener(QGraphicsSceneMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
    {
        bool ok;
        WGraphe * graph = d_parent;
        int sd = sommet_dep;
        int sa = sommet_arr;
        int rep = QInputDialog::getInt(nullptr, QString("Cout"), QString("Cout de l'arc en Km:"), d_cout, -9999, 9999, 1, &ok);
        if (ok)
        {
            grapheOV *gOV = nullptr;
            grapheNOV * gNOV = nullptr;

            gOV = dynamic_cast<grapheOV *>(graph->gcourant);
            gNOV = dynamic_cast<grapheNOV *>(graph->gcourant);


            int **cout = nullptr;

            if (gOV != nullptr)
            {
                cout = gOV->couts();
                cout[sd][sa] = rep;
                gOV->setCouts(cout);
            }
            else if (gNOV != nullptr)
            {
                cout = gNOV->couts();
                cout[sd][sa] = rep;
                gNOV->setCouts(cout);
            }
            graph->dessiner();

        }

    }
}
